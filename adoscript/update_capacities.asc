PROCEDURE global DELETE_ARRAY_ELEMENT elementid:integer {
    SETL i:(0)
    WHILE (i < (LEN tempPredecessors)) {
        IF (tempPredecessors[i]=elementid) {
            SET dummy: (aerase(tempPredecessors, i)) 
        }
        SETL i:(i+1)
    }
}

PROCEDURE global PRODUCE_PRODUCTS model:integer {
    CC "Core" GET_ALL_OBJS_OF_CLASSNAME modelid:(model) classname:"Producer"

    FOR id in:(objids) {
        CC "Core" GET_ATTR_VAL objid:(VAL id) attrname:"Supply"
        SET production:(val)

        CC "Core" GET_ATTR_VAL objid:(VAL id) attrname:"Current_Capacity"
        SET temp_capacity:(val)

        CC "Core" GET_ATTR_VAL objid:(VAL id) attrname:"Max_Capacity"
        SET max:(val)

        IF (temp_capacity+production<=max) {
            CC "Core" SET_ATTR_VAL objid:(VAL id) attrname:"Current_Capacity" val:(temp_capacity+production) 
        } ELSE {
            CC "Core" SET_ATTR_VAL objid:(VAL id) attrname:"Current_Capacity" val:(max) 
        }
    }
}

PROCEDURE global APPEND_PREDECESSORS sourceid:integer {
    CC "Core" GET_OBJ_NAME objid:(sourceid)

    CC "Core" GET_ATTR_VAL objid:(sourceid) attrname:"Current_Capacity"
    SET current_capacity:(val)
    CC "Core" GET_ATTR_VAL objid:(sourceid) attrname:"Demand"
    SET demand:(val)
    CC "Core" GET_ATTR_VAL objid:(sourceid) attrname:"Max_Capacity"
    SET max_capacity:(val)

    CC "Core" GET_CLASS_ID objid:(sourceid)
    CC "Core" GET_CLASS_NAME classid:(classid)
    IF (classname="Reseller") {
        IF (current_capacity>=demand) {
            SET current_capacity:(current_capacity-demand)
        } ELSE {
            SET current_capacity:(0)
        }
    }
    
    CC "Core" SET_ATTR_VAL objid:(sourceid) attrname:"Current_Capacity" val:(current_capacity)

    SET requested_goods:(max_capacity - current_capacity)

    CC "Core" GET_CONNECTORS objid:(sourceid)

    FOR selected in:(objids) {  

        CC "Core" GET_CONNECTOR_ENDPOINTS objid:(VAL selected)
        CC "Core" GET_OBJ_NAME objid:(fromobjid)
        CC "Core" GET_CLASS_ID objid:(fromobjid)
        CC "Core" GET_CLASS_NAME classid:(classid)

        IF (classname="DistributionHub") {
            
            CC "Core" GET_ATTR_VAL objid:(fromobjid) attrname:"Current_Capacity"
            SET temp_capacity:(val)

            IF (temp_capacity>=requested_goods) {
                CC "Core" SET_ATTR_VAL objid:(fromobjid) attrname:"Current_Capacity" val:(temp_capacity-requested_goods)
                CC "Core" GET_ATTR_VAL objid:(sourceid) attrname:"Current_Capacity"
                SET tempval:(val)
                CC "Core" SET_ATTR_VAL objid:(sourceid) attrname:"Current_Capacity" val:(tempval+requested_goods)
                SET requested_good:(0)     
            } ELSE {
                CC "Core" SET_ATTR_VAL objid:(fromobjid) attrname:"Current_Capacity" val:(0)
                CC "Core" GET_ATTR_VAL objid:(sourceid) attrname:"Current_Capacity"
                SET tempval:(val)
                CC "Core" SET_ATTR_VAL objid:(sourceid) attrname:"Current_Capacity" val:(tempval+temp_capacity)  
                SET requested_goods:(requested_goods-temp_capacity)
            }
            IF ((fromobjid IN tempPredecessors)=0) {
                SETG dummy:(aappend(tempPredecessors, fromobjid))
            }
            DELETE_ARRAY_ELEMENT elementid:(sourceid)
            
            
        } ELSIF (classname="Producer") {
            
            CC "Core" GET_ATTR_VAL objid:(fromobjid) attrname:"Current_Capacity"
            SET temp_capacity:(val)
            CC "Core" GET_ATTR_VAL objid:(fromobjid) attrname:"Supply"
            SET production:(val)
            IF (temp_capacity>=requested_goods) {
                CC "Core" SET_ATTR_VAL objid:(fromobjid) attrname:"Current_Capacity" val:(temp_capacity-requested_goods)
                CC "Core" GET_ATTR_VAL objid:(sourceid) attrname:"Current_Capacity"
                SET tempval:(val)
                CC "Core" SET_ATTR_VAL objid:(sourceid) attrname:"Current_Capacity" val:(tempval+requested_goods)
                SET requested_good:(0)
            } ELSE {
                CC "Core" SET_ATTR_VAL objid:(fromobjid) attrname:"Current_Capacity" val:(0)
                CC "Core" GET_ATTR_VAL objid:(sourceid) attrname:"Current_Capacity"
                SET tempval:(val)
                CC "Core" SET_ATTR_VAL objid:(sourceid) attrname:"Current_Capacity" val:(tempval+temp_capacity)  
                SET requested_goods:(requested_goods-temp_capacity)
            }
            
            
        } ELSIF (classname="Border") {
            CC "Core" GET_ATTR_VAL objid:(fromobjid) attrname:"Status"
            DELETE_ARRAY_ELEMENT elementid:(fromobjid)
            IF ((val)="Open") {
                
                CC "Core" GET_CONNECTORS objid:(fromobjid)

                FOR sel in:(objids) {  
                    CC "Core" GET_ATTR_VAL objid:(sourceid) attrname:"Current_Capacity"
                    SET current_capacity:(val)
                    #CC "AdoScript" INFOBOX (current_capacity)
                    CC "Core" GET_CONNECTOR_ENDPOINTS objid:(VAL sel)
                    CC "Core" GET_OBJ_NAME objid:(fromobjid)
                    CC "Core" GET_CLASS_ID objid:(fromobjid)
                    CC "Core" GET_CLASS_NAME classid:(classid)
                    IF (classname="DistributionHub") {
                        #CC "AdoScript" INFOBOX ("DistributionHub")
                        CC "Core" GET_ATTR_VAL objid:(fromobjid) attrname:"Current_Capacity"
                        SET temp_capacity:(val)

                        IF (temp_capacity>=requested_goods) {
                            CC "Core" SET_ATTR_VAL objid:(fromobjid) attrname:"Current_Capacity" val:(temp_capacity-requested_goods)
                            CC "Core" SET_ATTR_VAL objid:(sourceid) attrname:"Current_Capacity" val:(current_capacity+requested_goods)  
                            SET requested_goods:(0)       
                        } ELSE {
                            CC "Core" SET_ATTR_VAL objid:(fromobjid) attrname:"Current_Capacity" val:(0)
                            CC "Core" SET_ATTR_VAL objid:(sourceid) attrname:"Current_Capacity" val:(current_capacity+temp_capacity)     
                            SET requested_goods:(requested_goods-temp_capacity)
                        }
                        IF ((fromobjid IN tempPredecessors)=0) {
                            SETG dummy:(aappend(tempPredecessors, fromobjid))
                        }
                        DELETE_ARRAY_ELEMENT elementid:(sourceid)
                        
                        
                    } ELSIF (classname="Producer") {
                        #CC "AdoScript" INFOBOX ("producer")
                        CC "Core" GET_ATTR_VAL objid:(fromobjid) attrname:"Current_Capacity"
                        SET temp_capacity:(val)

                        #CC "AdoScript" INFOBOX (temp_capacity)
                        #CC "AdoScript" INFOBOX (requested_goods)
                        IF (temp_capacity>=requested_goods) {
                            CC "Core" SET_ATTR_VAL objid:(fromobjid) attrname:"Current_Capacity" val:(temp_capacity-requested_goods)
                            CC "Core" SET_ATTR_VAL objid:(sourceid) attrname:"Current_Capacity" val:(current_capacity+requested_goods)
                            SET requested_goods:(0)
                        } ELSE {
                            #CC "AdoScript" INFOBOX (current_capacity)
                            #CC "AdoScript" INFOBOX (temp_capacity)
                            #CC "AdoScript" INFOBOX (requested_goods)
                            CC "Core" SET_ATTR_VAL objid:(fromobjid) attrname:"Current_Capacity" val:(0)
                            CC "Core" SET_ATTR_VAL objid:(sourceid) attrname:"Current_Capacity" val:(current_capacity+temp_capacity)  
                            IF (requested_goods>=temp_capacity) {
                                SET requested_goods:(requested_goods-temp_capacity)
                            } ELSE {
                                SET requested_goods:(0)
                            }
                        }
                    }
                }
                
            }
  
        }
               
    }
    DELETE_ARRAY_ELEMENT elementid:(sourceid)
}

PROCEDURE global HAS_PREDECESSOR sourceid:integer {
    SETG hasPredecessor:(0)
    
    CC "Core" GET_OBJ_NAME objid:(sourceid)
    CC "Core" GET_CONNECTORS objid:(sourceid)

    FOR selected in:(objids) {  

        CC "Core" GET_CONNECTOR_ENDPOINTS objid:(VAL selected)
        CC "Core" GET_OBJ_NAME objid:(fromobjid)
        CC "Core" GET_CLASS_ID objid:(fromobjid)
        CC "Core" GET_CLASS_NAME classid:(classid)

        IF (classname="DistributionHub") {
            SETG hasPredecessor:(1)
            SETG tempid:(fromobjid)
        } ELSIF (classname="Producer") {
            SETG hasPredecessor:(0)
            SETG tempid:(fromobjid)
        } 
    }
}

PROCEDURE global UPDATE_AREAS {
    SEND "GET_ACTIVE_MODEL" to:"Modeling" answer:modelid
    CC "Core" GET_ALL_OBJS_OF_CLASSNAME modelid:(VAL modelid) classname:"Area"
    FOR areaid in:(objids) {
        CC "Core" GET_ATTR_VAL objid:(VAL areaid) attrname:"Name"
        SETL areaCurrentCapacity:(0)
        SETL areaMaxCapacity:(0)
        CC "Core" GET_CONNECTORS objid:(VAL areaid)

        FOR selected in:(objids) {  
            CC "Core" GET_CONNECTOR_ENDPOINTS objid:(VAL selected)
            
            CC "Core" GET_ATTR_VAL objid:(fromobjid) attrname:"Current_Capacity"
            SETL economicPartyCurrentCapacity:(val)
            
            SETL areaCurrentCapacity:(areaCurrentCapacity+economicPartyCurrentCapacity)

            CC "Core" SET_ATTR_VAL objid:(VAL areaid) attrname:"Current_Capacity" val:(areaCurrentCapacity)

            CC "Core" GET_ATTR_VAL objid:(fromobjid) attrname:"Max_Capacity"
            SETL economicPartyMaxCapacity:(val)
            
            CC "Core" GET_ATTR_VAL objid:(VAL areaid) attrname:"Max_Capacity"

           SETL areaMaxCapacity:(areaMaxCapacity+economicPartyMaxCapacity)

            CC "Core" SET_ATTR_VAL objid:(VAL areaid) attrname:"Max_Capacity" val:(areaMaxCapacity)

        }

        CC "Core" GET_ATTR_VAL objid:(VAL areaid) attrname:"Current_Capacity"
        SETL areaCurrentCapacity:(val)

        CC "Core" GET_ATTR_VAL objid:(VAL areaid) attrname:"Max_Capacity"
        SETL areaMaxCapacity:(val)

        IF (areaMaxCapacity!=0) {
            IF ((areaCurrentCapacity/areaMaxCapacity*100)<=20) {
                CC "Core" SET_ATTR_VAL objid:(VAL areaid) attrname:"Color" val:("red")
            } ELSIF ((areaCurrentCapacity/areaMaxCapacity*100)>20 AND (areaCurrentCapacity/areaMaxCapacity*100)<=50) {
                CC "Core" SET_ATTR_VAL objid:(VAL areaid) attrname:"Color" val:("darkorange")
            } ELSIF ((areaCurrentCapacity/areaMaxCapacity*100)>50 AND (areaCurrentCapacity/areaMaxCapacity*100)<=80) {
                CC "Core" SET_ATTR_VAL objid:(VAL areaid) attrname:"Color" val:("yellow")
            } ELSIF ((areaCurrentCapacity/areaMaxCapacity*100)>80) {
                CC "Core" SET_ATTR_VAL objid:(VAL areaid) attrname:"Color" val:("green")
            } 
        } ELSE {
            CC "Core" SET_ATTR_VAL objid:(VAL areaid) attrname:"Color" val:("black")
        }


    }
}



CC "AdoScript" EDITFIELD title:"Simulation days" caption:"Enter the number of days the simulation should capture:" text:"1"

SETL n:0
WHILE (n<(VAL text)) {

    # get current model
    SEND "GET_ACTIVE_MODEL" to:"Modeling" answer:modelid

    IF (LEN (modelid) = 0)
    {
    CC "AdoScript" ERRORBOX ("Please open a model!")
    EXIT
    }

    # get all objs of class
    CC "Core" GET_ALL_OBJS_OF_CLASSNAME modelid:(VAL modelid) classname:"Reseller"

    SETL all_ids:(objids)
    SETG tempPredecessors:(array(0))

    FOR id in:(all_ids) {
        SETG tempid:(VAL id)

        SETG dummy:(aappend(tempPredecessors, tempid))

        
        WHILE ((LEN tempPredecessors) > 0) {
            APPEND_PREDECESSORS sourceid:(tempid)

            IF ((LEN tempPredecessors) > 0) {
                SETG tempid:(tempPredecessors[0])
            }

            
        }

    }
    PRODUCE_PRODUCTS model:(VAL modelid)

    SEND "GET_ACTIVE_MODEL" to:"Modeling" answer:modelid

    CC "Core" GET_ALL_OBJS_OF_CLASSNAME modelid:(VAL modelid) classname:"Time"

    CC "Core" GET_ATTR_VAL objid:(VAL token (objids, 0, " ")) attrname:"ElapsedTime"
    SET time:(val)
    CC "Core" SET_ATTR_VAL objid:(VAL token (objids, 0, " ")) attrname:"ElapsedTime" val:(time+1)

    UPDATE_AREAS 

    CC "AdoScript" SLEEP ms:1000
    SETL n:(n+1)
}

   
